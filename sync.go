package main

import (
	"fmt"
	"os"
	"strings"
)

func syncup() {
	notf := true
	environment := strings.Split(os.Getenv("PATH"), ":")
	for _, v := range environment {
		if _, err := os.Stat(v + "/terraform"); err == nil {
			notf = false
		}
	}
	if notf == true {
		fmt.Printf("Environment not setup! Starting initialization!\n")
		getTf()
	}
}
