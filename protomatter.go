package main

import (
	"fmt"
	"os/exec"
	"strings"
	//3rd party stuff here
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	sync   = kingpin.Flag("sync", "Get latest version of terraform").Short('s').Bool()
	path   = kingpin.Flag("path", "Set the path for your terraform files").Short('p').String()
	initme = kingpin.Flag("init", "Setup Protomatter environment automatically").Bool()
)

func main() {
	kingpin.Parse()
	if *initme == true {
		fmt.Println("Starting Initialization process")
		getTf()
	}
	if *sync == true {
		syncup()
	}
	walktheplank()
}

func execute(commandString string) error {
	commandSlice := strings.Fields(commandString)
	c := exec.Command(commandSlice[0], commandSlice[1:]...)
	e := c.Run()
	fmt.Printf("E has: %v\n", e)
	if e != nil {
		return e
	}
	return e
}
