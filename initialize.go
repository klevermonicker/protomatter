package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func getTf() {
	path := os.Getenv("HOME")
	out, err := os.Create(path + "/.local/bin/terraform.zip")
	if err != nil {
		fmt.Println(err)
	}
	defer out.Close()

	resp, err := http.Get("https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_darwin_amd64.zip")
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	setupTf(path + "/.local/bin")
}

func setupTf(path string) {
	commandString := fmt.Sprintf(`unzip %s -d %s`, path+"/terraform.zip", path)
	execute(commandString)
	tfCleanup(path + "/terraform.zip")
}

func tfCleanup(path string) {
	os.Remove(path)
	setupPath()
}

func setupPath() {
	newpath := os.Getenv("HOME") + "/.protomatter"
	input := bufio.NewReader(os.Stdin)
	fmt.Print("Enter the path where your terraform files are kept.\n")
	fmt.Print("Path: ")
	text, _ := input.ReadString('\n')
	if strings.Contains(text, "~") {
		err := ioutil.WriteFile(newpath, []byte(strings.Replace(text, "~", os.Getenv("HOME"), 1)), 0755)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Protomatter environment successfully setup. To change your terraform project path rerun protomatter with --init flag")
			fmt.Println("Protomatter environment location: " + os.Getenv("HOME") + "/.protomatter")
		}
	} else {
		err := ioutil.WriteFile(newpath, []byte(text), 0755)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Protomatter environment successfully setup. To change your terraform project path rerun protomatter with --init flag")
			fmt.Println("Protomatter environment location: " + os.Getenv("HOME") + "/.protomatter")
		}
	}
	setupShell()
}

func setupShell() {
	shellpath := strings.Split(os.Getenv("SHELL"), "/")
	shell := shellpath[len(shellpath)-1]
	fmt.Println("Add this to your path in your shell")
	fmt.Println("PATH=$PATH:" + os.Getenv("HOME") + "/.local/bin")
	if shell == "zsh" {
		fmt.Println("or run the following command")
		fmt.Println("echo \"PATH=$PATH:" + os.Getenv("HOME") + "/.local/bin\" >> ~/.zshrc")
	} else if shell == "bash" {
		fmt.Println("or run the following command")
		fmt.Println("echo \"PATH=$PATH:" + os.Getenv("HOME") + "/.local/bin\" >> ~/.bashrc")
	}
}
