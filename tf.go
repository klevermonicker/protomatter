package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func runTf(path string) {
	fmt.Println("Running Terraform")
	if strings.Contains(path, "lambda") {
		trim := strings.Trim(path, "lambda")
		slashTrim := strings.TrimRight(trim, "/")
		path = slashTrim
	}
	commands := []string{"terraform init", "terraform plan", "terraform apply -auto-approve"}
	for _, cmds := range commands {
		tfExec(path, cmds)
	}
}

func tfExec(path string, command string) {
	commandSlice := strings.Fields(command)
	tf := exec.Command(commandSlice[0], commandSlice[1:]...)
	tf.Dir = path
	out, err := tf.CombinedOutput()
	if err != nil {
		fmt.Println(string(out))
		os.Exit(1)
	}
}
