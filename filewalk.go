package main

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func walktheplank() {
	if strings.Contains(*path, "/") {
		walkFn(*path)
	} else {
		dat, err := ioutil.ReadFile(os.Getenv("HOME") + "/.protomatter")
		if err != nil {
			fmt.Println(err)
		}
		walkFn(string(dat))
	}
}

func walkFn(path string) {
	lambdaPath := strings.TrimSpace(path) + "/lambda"
	fmt.Printf("Discovering Lambda functions in: %s\n", lambdaPath)
	err := filepath.Walk(lambdaPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("%q \n", path)
			return err
		}
		if info.IsDir() && info.Name() != "lambda" {
			zipLambda(lambdaPath, info.Name())
		}
		runTf(path)
		return nil
	})
	if err != nil {
		fmt.Printf("Problem walking the path: %v\n", err)
	}
}

func zipLambda(lambdaPath string, zipdir string) {
	lambdaPath = lambdaPath + "/" + zipdir
	files, err := ioutil.ReadDir(lambdaPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	os.Chdir(lambdaPath)
	newZip, err := os.Create(lambdaPath + "/update_" + zipdir + ".zip")
	if err != nil {
		fmt.Println(err)
	}
	defer newZip.Close()
	zipme := zip.NewWriter(newZip)
	defer zipme.Close()
	for _, f := range files {
		if strings.HasSuffix(f.Name(), "zip") != true {
			zipfile, err := os.Open(f.Name())
			if err != nil {
				fmt.Println(err)
			}
			defer zipfile.Close()
			info, err := zipfile.Stat()
			if err != nil {
				fmt.Println(err)
			}
			header, err := zip.FileInfoHeader(info)
			if err != nil {
				fmt.Println(err)
			}
			header.Name = f.Name()
			header.Method = zip.Deflate
			writer, err := zipme.CreateHeader(header)
			if err != nil {
				fmt.Println(err)
			}
			if _, err = io.Copy(writer, zipfile); err != nil {
				fmt.Println(err)
			}
		}
	}
}
